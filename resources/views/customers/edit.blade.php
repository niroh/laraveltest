<h1>Edit Customers Details</h1>
<form method = 'post' action="{{action('CustomersController@update', $customer->id)}}">
@csrf
@method('PATCH')
<div class = "form-group">
    <label for = "title">Customer to Update</label>
    <br>
    <br>
    <input type= "text" class = "form-control" name= "name" value = "{{$customer->name}}">
    <br>
    <br>
    <input type= "text" class = "form-control" name= "email" value = "{{$customer->email}}">
    <br>
    <br>
    <input type= "text" class = "form-control" name= "phone" value = "{{$customer->phone}}">
    <br>
    <br>
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Save Changes">
</div>

</form>
