<h1>Create New customer</h1>
<form method = 'post' action="{{action('CustomersController@store')}}">
{{csrf_field()}}

<div class = "form-group">
    <label for = "title">Enter customer's details</label>
    <br>
    <br>
    Name: <input type= "text" class = "form-control" name= "name">
    <br>
    <br>
    Email Address: <input type= "text" class = "form-control" name= "email">
    <br>
    <br>
    Phone Number: <input type= "text" class = "form-control" name= "phone">
    <br>
    <br>
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Save">
</div>

</form>
