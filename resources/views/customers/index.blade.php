<h1>This is your customers list</h1>
<a href="{{route('customers.create')}}">Create new customer </a>
<ul>
@foreach($customers as $customer)   
    <li> 
       Customer's Name: {{$customer->name}} 
       Customer's Email Address: {{$customer->email}} 
       Customer's Phone Number: {{$customer->phone}} 
       Created By:  {{$customer->user_id}}      
       <a href= "{{route('customers.edit', $customer->id )}}"> Edit Customer </a>
        @if($customer->status == 1)
            <a></a>
            @else
            @cannot('salesrep')<a input type ='url' href= "{{route('update', $customer->id)}}"> Deal Close </a> @endcannot
            @endif

        @if($creator->role == 'salesrep')
            <a>Delete Customer</a>
            @else
            <a href= "{{route('delete', $customer->id)}}"> Delete Customer </a>
            @endif
    </li>
    @endforeach
</ul>